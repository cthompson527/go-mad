package gomad

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"

	"github.com/jinzhu/gorm"
	// postgres dialect in gorm
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"github.com/jinzhu/inflection"
)

// DB is the exported DB variable to be used throughout the application
var DB *gorm.DB

// GetDB setups the DB and saves it to the exported DB variable
func GetDB() (*gorm.DB, error) {
	host := os.Getenv("PG_HOST")
	port := os.Getenv("PG_PORT")
	name := os.Getenv("PG_DBNAME")
	username := os.Getenv("PG_USERNAME")
	password := os.Getenv("PG_PASSWORD")
	environment := os.Getenv("APP_ENV")
	var connectionString string
	if username == "" {
		connectionString = fmt.Sprintf("host=%s port=%s dbname=%s sslmode=disable", host, port, name)
	} else {
		connectionString = fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=require", host, port, name, username, password)
	}
	db, err := gorm.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}

	if environment != "production" {
		db.LogMode(true)
	}

	DB = db
	return db, nil
}

// Many2ManyFIndex creates the foreign key relationships between the two models
func Many2ManyFIndex(parentModel interface{}, childModel interface{}) {
	table1Accessor := reduceModelToName(parentModel)
	table2Accessor := reduceModelToName(childModel)

	table1Name := inflection.Plural(table1Accessor)
	table2Name := inflection.Plural(table2Accessor)

	joinTable := fmt.Sprintf("%s_%s", table1Accessor, table2Name)

	DB.Table(joinTable).AddForeignKey(table1Accessor+"_"+table1Accessor+"_id", table1Name+"("+table1Accessor+"_id)", "CASCADE", "CASCADE")
	DB.Table(joinTable).AddForeignKey(table2Accessor+"_"+table2Accessor+"_id", table2Name+"("+table2Accessor+"_id)", "CASCADE", "CASCADE")
	DB.Table(joinTable).AddUniqueIndex(joinTable+"_unique", table1Accessor+"_"+table1Accessor+"_id", table2Accessor+"_"+table2Accessor+"_id")
}

func reduceModelToName(model interface{}) string {
	value := reflect.ValueOf(model)
	if value.Kind() != reflect.Ptr {
		return ""
	}

	elem := value.Elem()
	t := elem.Type()
	rawName := t.Name()
	return gorm.ToDBName(rawName)
}

type environments struct {
	Testing     environment `json:"testing"`
	Development environment `json:"development"`
	Production  environment `json:"production"`
}

type environment struct {
	Host     string `json:"PG_HOST"`
	Port     string `json:"PG_PORT"`
	Name     string `json:"PG_DBNAME"`
	Username string `json:"PG_USERNAME"`
	Password string `json:"PG_PASSWORD"`
	Jwt      string `json:"JWT_SECRET"`
}

// SetEnvironmentVariables will set the environment variables based
// on two parameters, the path to the json env file and the env to set
func SetEnvironmentVariables(path string, env string) error {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	var envs environments
	err = json.Unmarshal(b, &envs)
	if err != nil {
		return err
	}

	var environ environment

	switch env {
	case "development":
		{
			environ = envs.Development
		}
	case "production":
		{
			environ = envs.Production
		}
	case "testing":
		{
			environ = envs.Testing
		}
	}

	return setEnvironmentVariables(environ)
}

func setEnvironmentVariables(env environment) error {
	if err := os.Setenv("PG_HOST", env.Host); err != nil {
		return err
	}

	if err := os.Setenv("PG_PORT", env.Port); err != nil {
		return err
	}

	if err := os.Setenv("PG_DBNAME", env.Name); err != nil {
		return err
	}

	if err := os.Setenv("PG_USERNAME", env.Username); err != nil {
		return err
	}

	if err := os.Setenv("PG_PASSWORD", env.Password); err != nil {
		return err
	}

	if err := os.Setenv("JWT_SECRET", env.Jwt); err != nil {
		return err
	}

	return nil
}
