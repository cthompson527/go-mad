package gomad

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	uuid "github.com/satori/go.uuid"
)

// Credentials is the table storing the user's password
type Credentials struct {
	CredentialID uuid.UUID `gorm:"type:uuid; primary_key"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time `sql:"index"`
	Password     string     `sql:"index"`
	UserID       uuid.UUID  `gorm:"type:uuid REFERENCES users(user_id)"`
	User         User
}

// Token is the JWT token to define the user's session
type Token struct {
	Token string
}

// TOKEN returns the string representation of the Token
func (t Token) TOKEN(ctx context.Context) string {
	return t.Token
}

// LoginInput is what the user submits in the login form
type LoginInput struct {
	UserEmail    string
	UserPassword string
}

// SignupInput is the required fields the user needs to submit in order to sign up
type SignupInput struct {
	UserEmail    string
	UserPassword string
	FirstName    string
	LastName     string
}

// SignJWT creates the JWT token with the signed signature, returning the new token string
func SignJWT(user *User) (*string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":         base64.StdEncoding.EncodeToString([]byte(user.UserID.String())),
		"created_at": user.CreatedAt,
		"exp":        time.Now().Add(time.Second * 86400).Unix(), // exp in one day
		"iss":        "gomad",
	})

	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	return &tokenString, err
}

// Authenticate verifies the JWT token is a valid token, with a valid signed key. It sets
// the context of the request based on the validation.
func Authenticate(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var (
			isAuthorized = false
			userID       string
		)
		ctx := r.Context()
		token, err := validateBearerAuthHeader(ctx, r)
		if err == nil {
			isAuthorized = true
			if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
				userIDByte, _ := base64.StdEncoding.DecodeString(claims["id"].(string))
				userID = string(userIDByte[:])

				uID, errUUID := uuid.FromString(userID)
				if errUUID == nil {
					user := User{UserID: uID}
					user.UpdateLastActive()
				}
			} else {
				log.Println(err)
			}
		}
		ctx = context.WithValue(ctx, "user_id", &userID)            // nolint
		ctx = context.WithValue(ctx, "is_authorized", isAuthorized) // nolint
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

func validateBearerAuthHeader(ctx context.Context, r *http.Request) (*jwt.Token, error) {
	var tokenString string
	auth := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
	if len(auth) != 2 || auth[0] != "Bearer" {
		return nil, fmt.Errorf("no bearer token")
	}
	tokenString = auth[1]

	token, err := validateJWT(&tokenString)
	return token, err
}

func validateJWT(tokenString *string) (*jwt.Token, error) {
	token, err := jwt.Parse(*tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("	unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(os.Getenv("JWT_SECRET")), nil
	})
	return token, err
}
