package gomad_test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"strings"
	"testing"

	"github.com/satori/go.uuid"

	"github.com/jinzhu/gorm"
	"github.com/phayes/freeport"

	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"

	"gitlab.com/cthompson527/go-mad/gomad"
)

func TestMain(m *testing.M) {
	if os.Getenv("CI") != "Y" {
		err := gomad.SetEnvironmentVariables("../env.json", "testing")
		checkErr(err)
	}

	db, err := gomad.GetDB()
	checkErr(err)

	deleteAllTables(db)

	db.AutoMigrate(&gomad.User{})
	db.AutoMigrate(&gomad.League{})
	gomad.Many2ManyFIndex(&gomad.User{}, &gomad.League{})
	db.AutoMigrate(&gomad.Credentials{})

	s, err := gomad.GetSchemaOf("../schema.graphql")
	checkErr(err)
	schema := graphql.MustParseSchema(s, &gomad.Query{})

	port, err := freeport.GetFreePort()
	checkErr(err)
	os.Setenv("API_PORT", fmt.Sprintf("%d", port))

	server := httptest.NewServer(gomad.Authenticate(&relay.Handler{Schema: schema}))
	os.Setenv("API_URL", server.URL)

	os.Exit(m.Run())
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

type Variables map[string]interface{}

var queries = []struct {
	Name      string
	Query     string
	Variables Variables
	Resp      []byte
	SendToken bool
}{
	{
		"unauthorized",
		removeInvalid(EmptyResponseAndFirstNameQuery),
		Variables{"email": "a@b.com"},
		[]byte(`{"data":{"user":null},"errors":[{"message":"user is not authorized to perform this query","path":["user"]}]}`),
		false,
	},
	{
		"empty response",
		removeInvalid(EmptyResponseAndFirstNameQuery),
		Variables{"email": "a@b.com"},
		[]byte(`{"data":{"user":null},"errors":[{"message":"could not find user a@b.com: record not found","path":["user"]}]}`),
		true,
	},
	{
		"add user mutation",
		removeInvalid(AddUserMutationQuery),
		Variables{"email": "a@b.com", "firstName": "Cory", "lastName": "Thompson"},
		[]byte(`{"data":{"addUser":true}}`),
		true,
	},
	{
		"firstName response",
		removeInvalid(EmptyResponseAndFirstNameQuery),
		Variables{"email": "a@b.com"},
		[]byte(`{"data":{"user":{"firstName":"Cory", "lastName": "Thompson", "email": "a@b.com", "deleted_at": null}}}`),
		true,
	},
	{
		"add league mutation",
		removeInvalid(AddLeagueMutationQuery),
		Variables{"name": "1st and 30"},
		[]byte(`{"data":{"addLeague":true}}`),
		true,
	},
	{
		"add user to league mutation",
		removeInvalid(AddUserToLeagueMutationQuery),
		Variables{"userEmail": "a@b.com", "leagueName": "1st and 30"},
		[]byte(`{"data":{"addUserToLeague":true}}`),
		true,
	},
	{
		"signup mutation",
		removeInvalid(SignupMutationQuery),
		Variables{"userEmail": "b@c.com", "userPassword": "abcd1234", "firstName": "Cory", "lastName": "Thompson"},
		[]byte(`{"data":{"signup":true}}`),
		false,
	},
	{
		"login mutation fail",
		removeInvalid(LoginMutationQuery),
		Variables{"userEmail": "b@c.com", "userPassword": "abcd123"},
		[]byte(`{"data":{"login":null},"errors":[{"message":"invalid password for user b@c.com, crypto/bcrypt: hashedPassword is not the hash of the given password","path":["login"]}]}`),
		false,
	},
	{
		"query user and leagues",
		removeInvalid(UserWithLeaguesWithUsersQuery),
		Variables{"email": "a@b.com"},
		[]byte(`{"data":{"user":{"firstName":"Cory","leagues":[{"name":"1st and 30","deleted_at":null,"users":[{"firstName":"Cory"}]}]}}}`),
		true,
	},
}

func TestLeagues(t *testing.T) {
	userID, err := uuid.NewV4()
	checkErr(err)
	token, err := gomad.SignJWT(&gomad.User{UserID: userID})
	checkErr(err)
	for _, tt := range queries {
		client := &http.Client{}
		requestURI := fmt.Sprintf("%s/query", os.Getenv("API_URL"))
		t.Run(tt.Name, func(t *testing.T) {
			b, err := json.Marshal(tt.Variables)
			checkErr(err)
			variables := string(b)
			requestBody := strings.NewReader(`{"query":` + tt.Query + `,"variables": ` + variables + "}")
			fmt.Println(requestBody)

			req, err := http.NewRequest(http.MethodPost, requestURI, requestBody)
			checkErr(err)
			if tt.SendToken {
				req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", *token))
			}

			resp, err := client.Do(req)
			checkErr(err)
			defer resp.Body.Close()

			body, err := ioutil.ReadAll(resp.Body)
			checkErr(err)

			if ok, err := jSONBytesEqual(body, tt.Resp); !ok || err != nil {
				t.Errorf("expected: %s, got: %s", tt.Resp, string(body))
			}
		})
	}
}

func TestNondeterministics(t *testing.T) {
	client := &http.Client{}
	requestURI := fmt.Sprintf("%s/query", os.Getenv("API_URL"))
	b, err := json.Marshal(Variables{"userEmail": "b@c.com", "userPassword": "abcd1234"})
	checkErr(err)
	variables := string(b)
	requestBody := strings.NewReader(`{"query":` + removeInvalid(LoginMutationQuery) + `,"variables": ` + variables + "}")
	fmt.Println(requestBody)

	req, err := http.NewRequest(http.MethodPost, requestURI, requestBody)
	checkErr(err)

	resp, err := client.Do(req)
	checkErr(err)
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	checkErr(err)

	if !strings.Contains(string(body), `{"data":{"login":{"token":`) {
		t.Errorf("nondeterministic(%s): expected login to pass, but it failed: %s", "login mutation pass", string(body))
	}
}

func deleteAllTables(db *gorm.DB) {
	db.DropTableIfExists("user_leagues")
	db.DropTableIfExists(&gomad.Credentials{})
	db.DropTableIfExists(&gomad.User{})
	db.DropTableIfExists(&gomad.League{})
}

func removeInvalid(s string) string {
	s = strings.Replace(s, "\n", " ", -1)
	return strings.Replace(s, "\t", " ", -1)
}

// jSONBytesEqual compares the JSON in two byte slices.
func jSONBytesEqual(a, b []byte) (bool, error) {
	var j, j2 interface{}
	if err := json.Unmarshal(a, &j); err != nil {
		return false, err
	}
	if err := json.Unmarshal(b, &j2); err != nil {
		return false, err
	}
	return reflect.DeepEqual(j2, j), nil
}

const EmptyResponseAndFirstNameQuery = `
"query($email: String!) {
	user(email: $email) {
		firstName
		lastName
		email
		deleted_at
	}
}"
`

const AddUserMutationQuery = `
"mutation($email: String!, $firstName: String!, $lastName: String!) {
	addUser(input: { email: $email, firstName: $firstName, lastName: $lastName })
}"
`

const AddLeagueMutationQuery = `
"mutation($name: String!) {
	addLeague(input: { name: $name })
}"
`

const AddUserToLeagueMutationQuery = `
"mutation($userEmail: String!, $leagueName: String!) {
	addUserToLeague(input: { userEmail: $userEmail, leagueName: $leagueName })
}"
`

const SignupMutationQuery = `
"mutation($userEmail: String!, $userPassword: String!, $firstName: String!, $lastName: String!) {
  signup(input: { userEmail: $userEmail, userPassword: $userPassword, firstName: $firstName, lastName: $lastName })
}"
`

const LoginMutationQuery = `
"mutation($userEmail: String!, $userPassword: String!){
  login(input: { userEmail: $userEmail, userPassword: $userPassword }) {
    token
  }
}"
`

const UserWithLeaguesWithUsersQuery = `
"query($email: String!) {
  user(email: $email) {
    firstName
    leagues {
			name
			deleted_at
      users {
        firstName
      }
    }
  }
}"
`
