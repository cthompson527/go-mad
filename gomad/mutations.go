package gomad

import (
	"context"
	"fmt"
	"time"

	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

// AddUser accepts the UserInput argument and adds the new user to the database, returning true if successful
func (q *Query) AddUser(ctx context.Context, args struct{ Input UserInput }) (bool, error) {
	gid, err := uuid.NewV4()
	if err != nil {
		return false, fmt.Errorf("could not create an uuid: %v", err)
	}

	user := User{
		UserID:    gid,
		FirstName: args.Input.FirstName,
		LastName:  args.Input.LastName,
		Email:     args.Input.Email,
	}
	db := DB.Create(&user)
	if db.Error != nil {
		return false, db.Error
	}

	return true, nil
}

// AddLeague accepts the LeagueInput argument and adds a new league to the database, returning true if successful
func (q *Query) AddLeague(ctx context.Context, args struct{ Input LeagueInput }) (bool, error) {
	gid, err := uuid.NewV4()
	if err != nil {
		return false, fmt.Errorf("could not create an uuid: %v", err)
	}

	league := League{
		LeagueID: gid,
		Name:     args.Input.Name,
	}
	db := DB.Create(&league)
	if db.Error != nil {
		return false, db.Error
	}

	return true, nil
}

// AddUserToLeague adds an already defined user to a league
func (q *Query) AddUserToLeague(ctx context.Context, args struct{ Input UserToLeagueInput }) (bool, error) {
	var league League
	if err := DB.First(&league, "name=?", args.Input.LeagueName).Error; err != nil {
		return false, fmt.Errorf("could not find league %s, %v", args.Input.LeagueName, err)
	}

	var user User
	if err := DB.First(&user, "email=?", args.Input.UserEmail).Error; err != nil {
		return false, fmt.Errorf("could not find user %s, %v", args.Input.UserEmail, err)
	}

	DB.Model(&user).Association("Leagues").Append(&league)

	return true, nil
}

// Login is called upon login. Login verifies the user's email and password and, if valid, returns a JWT token
func (q *Query) Login(ctx context.Context, args struct{ Input LoginInput }) (*Token, error) {
	var user User
	if err := DB.First(&user, "email=?", args.Input.UserEmail).Error; err != nil {
		return nil, fmt.Errorf("could not find user %s, %v", args.Input.UserEmail, err)
	}

	var credentials Credentials
	if err := DB.First(&credentials, "user_id=?", user.UserID).Error; err != nil {
		return nil, fmt.Errorf("could not find password for user %s, %v", args.Input.UserEmail, err)
	}

	pass := []byte(credentials.Password)
	submittedPass := []byte(args.Input.UserPassword)

	if err := bcrypt.CompareHashAndPassword(pass, submittedPass); err != nil {
		return nil, fmt.Errorf("invalid password for user %s, %v", args.Input.UserEmail, err)
	}

	token, err := SignJWT(&user)
	if err != nil {
		return nil, fmt.Errorf("unable to sign jwt token: %v", err)
	}

	return &Token{Token: *token}, nil
}

// Signup is the function used called when the user signs up for our application. It creates the user and the user's password
func (q *Query) Signup(ctx context.Context, args struct{ Input SignupInput }) (bool, error) {
	password, err := bcrypt.GenerateFromPassword([]byte(args.Input.UserPassword), 10)
	if err != nil {
		return false, fmt.Errorf("could not bcrypt password: %v", err)
	}

	credentialID, err := uuid.NewV4()
	if err != nil {
		return false, fmt.Errorf("could not generate credentialID: %v", err)
	}

	userID, err := uuid.NewV4()
	if err != nil {
		return false, fmt.Errorf("could not generate userID: %v", err)
	}

	timeNow := time.Now()

	user := User{
		UserID:     userID,
		FirstName:  args.Input.FirstName,
		LastName:   args.Input.LastName,
		Email:      args.Input.UserEmail,
		LastActive: &timeNow,
	}

	if err = DB.Create(&user).Error; err != nil {
		return false, fmt.Errorf("could not find user %s, %v", args.Input.UserEmail, err)
	}

	credentials := Credentials{
		CredentialID: credentialID,
		Password:     string(password),
		UserID:       userID,
	}

	if err = DB.Create(&credentials).Error; err != nil {
		return false, fmt.Errorf("could not find user %s, %v", args.Input.UserEmail, err)
	}

	return true, err
}
