package gomad

import (
	"context"
	"time"

	uuid "github.com/satori/go.uuid"
)

// League is the connection of users to compete with one another
type League struct {
	LeagueID  uuid.UUID `gorm:"type:uuid; primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	Name      string     `gorm:"type:varchar(100);unique_index"`
	Users     *[]*User   `gorm:"many2many:user_leagues;"`
}

// LeagueInput is the input for mutations
type LeagueInput struct {
	Name string
}

// UserToLeagueInput is the GraphQL input for mutations
type UserToLeagueInput struct {
	UserEmail  string
	LeagueName string
}

// LEAGUEID will return the uuid of the league
func (l *League) LEAGUEID(ctx context.Context) string {
	return l.LeagueID.String()
}

// NAME will return the name of the league
func (l *League) NAME(ctx context.Context) string {
	return l.Name
}

// CREATED_AT returns the time the league was created
func (l *League) CREATED_AT(ctx context.Context) string { //nolint
	return l.CreatedAt.String()
}

// UPDATED_AT returns the time the league was last updated
func (l *League) UPDATED_AT(ctx context.Context) string { // nolint
	return l.UpdatedAt.String()
}

// DELETED_AT returns the time the league was deleted
func (l *League) DELETED_AT(ctx context.Context) *string { //nolint
	deletedAt := l.DeletedAt
	if deletedAt == nil {
		return nil
	}
	return StrP(l.DeletedAt.String())
}

// USERS will return an array of users in the league
func (l *League) USERS(ctx context.Context) *[]*User {
	var users []*User
	DB.Model(l).Association("Users").Find(&users)
	return &users
}
