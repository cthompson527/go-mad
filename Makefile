PROJECT_NAME := "go-mad"
PKG := "gitlab.com/cthompson527/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: ## Lint the files
	gometalinter ./...

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

dep_test: ## Get the dependencies needed to run the tests
	@go get -v -t -d ./...

dep: ## Get the dependencies
	@go get -v -d ./...

build: dep ## Build the binary file
	@go build -i -v $(PKG)

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)
	@rm cover*

docker_build: ## Build the Docker image with the app inside
	GOOS=linux go build -o app
	docker build -t registry.gitlab.com/cthompson527/$(PROJECT_NAME) .
	rm -f app

docker_run: docker_build ## Run the docker image
	docker run -p 8080:8080 -e PG_USERNAME=coryathompson -e PG_PASSWORD=abcd1234 -e PG_HOST=192.168.1.150 -e PG_DBNAME=ncaa-madness -e PG_PORT=5432 -e APP_ENV=production registry.gitlab.com/cthompson527/madness/$(PROJECT_NAME)

docker_push: docker_build ## Push the image to GitLab's registry
	docker push registry.gitlab.com/cthompson527/$(PROJECT_NAME)

run: ## Run the application with development environment
	APP_ENV=development go run main.go

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
