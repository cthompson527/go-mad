FROM alpine

WORKDIR /app

ADD app app
ADD schema.graphql .

EXPOSE 8080

CMD [ "./app" ]